FROM ubuntu:20.04
RUN apt-get update
RUN apt install software-properties-common -y
RUN add-apt-repository ppa:ondrej/php -y
RUN apt install python3-pip -y
RUN apt install php8.0 -y
RUN pip3 install awscli
ADD enablessh.sh /usr/sbin/enablessh
RUN chmod +x /usr/sbin/enablessh 
